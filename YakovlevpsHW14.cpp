
#include <iostream>
#include <string>
#include <iomanip>

int main()
{

	std::cout << "Please, type any word: " << "\n";
	std::string HomeWork,First,Last;
	std::cin >> HomeWork;

	std::cout << "Word :" << HomeWork << "\n";
	std::cout << "Length of word :" << HomeWork.length() << "\n";

	int L = HomeWork.length() - 1;

	First = HomeWork[0];
	Last = HomeWork[L];
	
	std::cout << "First symbol :" << First << "\n";
	std::cout << "Last symbol :" << Last << "\n";
	
		return 0;
}

